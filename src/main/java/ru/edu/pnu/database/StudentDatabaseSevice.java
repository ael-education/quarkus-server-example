/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.edu.pnu.database;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;
import ru.edu.pnu.entity.Student;

/**
 *  Класс для создания удаления и обновления записей о студентах
 * 
 * @author developer
 */
@ApplicationScoped
public class StudentDatabaseSevice {
    
    private static final Logger log = Logger.getLogger(StudentDatabaseSevice.class.getName());
    
     @Inject
     EntityManager em;
     
     /**
      * Сохранение студента в базе данных
      * @param student 
      */
     
     @Transactional
     public Student saveStudent (Student student)
     {
         em.persist(student);
         log.info("Сохранен студент , присвоен идентифкатор: ["+student.getId()+"]");        
         return student;
     }        
             
 
     /**
      * Получение полного списка студентов
      * @param student
      * @return 
      */
     
    @Transactional
    public List<Student> getAllStudents() {
        log.info("Получение полного списка студентов ....");

        // Строка запроса на языке JPQL
        String request = "Select student from " + Student.class.getSimpleName() + " student";

        List<Student> studentList = new ArrayList<>();

        studentList = (List<Student>) em.createQuery(request).getResultList();

        log.info("Получен список студентов из БД [" + studentList.size() + "] записей");

        return studentList;
    }
     
    
}
