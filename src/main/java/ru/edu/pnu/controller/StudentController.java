package ru.edu.pnu.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import org.jboss.logging.Logger;
import ru.edu.pnu.entity.Student;
import ru.edu.pnu.database.StudentDatabaseSevice;

@Path("/student")
public class StudentController {

    private static final Logger log = Logger.getLogger(StudentController.class.getName());
    
    @Inject
    StudentDatabaseSevice studentDatabaseSevice;
    
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Student> getStudentList() {
         log.info("Поступил запрос на получение полного списка студентов ...");
        
        List<Student> allStudents = studentDatabaseSevice.getAllStudents();
        
        return allStudents;
    }
    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Student saveStudent(Student student) {
        
        log.info("Сохранение студена в БД ...");
        
        Student savedStudent = studentDatabaseSevice.saveStudent(student);
        
        
        return savedStudent;
    }
    
}
